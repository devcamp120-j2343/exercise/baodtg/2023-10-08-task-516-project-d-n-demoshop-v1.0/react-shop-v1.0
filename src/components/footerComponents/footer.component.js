import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'

class FooterComponent extends Component {
    render() {
        return (
            <>
                <div className="footer container-fluid bg-dark pt-3 mt-5 text-white">
                    <div className="row">
                        <div className="col-5 " style={{ paddingLeft: "150px", paddingRight: "150px" }}>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>©  2018 . All Rights Reserved.</p>
                        </div>
                        <div className="col-4" style={{}}>
                            <div className="row">
                                <h5 >Contacts</h5>
                                <div className="col-12">
                                    <h6>Address:</h6>
                                    <p>Kolkata, West Bengal, India
                                    </p>
                                </div>
                                <div className="col-12">
                                    <h6>email:</h6>
                                    <p>info@example.com
                                    </p>
                                </div>
                                <div className="col-12">
                                    <h6>phones:</h6>
                                    <p>+91 99999999 or +91 11111111
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-3" style={{}}>
                            <div className="row">
                                <h5 >Links</h5>
                                <div className="col-12">
                                    <p>About
                                    </p>
                                </div>
                                <div className="col-12">
                                    <p>Projects
                                    </p>
                                </div>
                                <div className="col-12">
                                    <p>Blog
                                    </p>
                                </div>
                                <div className="col-12">
                                    <p>Contacts
                                    </p>
                                </div>
                                <div className="col-12">
                                    <p>Pricing
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col-3 text-center pb-3 pt-3" style={{ border: "1px solid grey" }}>
                            <h5>FACEBOOK</h5>
                        </div>
                        <div className="col-3 text-center pb-3 pt-3" style={{ border: "1px solid grey" }}>
                            <h5>INSTAGRAM</h5>

                        </div>
                        <div className="col-3 text-center pb-3 pt-3" style={{ border: "1px solid grey" }}>
                            <h5>TWITER</h5>

                        </div>
                        <div className="col-3 text-center pb-3 pt-3" style={{ border: "1px solid grey" }}>
                            <h5>GOOGLE</h5>

                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default FooterComponent