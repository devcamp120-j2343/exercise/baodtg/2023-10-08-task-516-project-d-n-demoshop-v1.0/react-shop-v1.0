import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import mainLogo from '../../assets/images/React_logo_logotype_emblem-700x626.webp'
import { Navbar, Nav } from 'react-bootstrap';

class HeaderComponent extends Component {
    render() {
        return (
            <>
                <div className="container">
                    <div className="row">
                        <div className="col-2 pt-4 " >
                            <img src={mainLogo} alt="logo" width={"50px"} style={{ marginLeft: "100px" }}></img>
                        </div>
                        <div className="col-10 pt-2 text-center">
                            <h1>React Store</h1>
                            <p>Demo App Shop24h v1.0</p>
                        </div>
                    </div>
                </div>
                <div className="container-fluid bg-dark" style={{height: "60px" }}>
                    <Navbar bg='dark' expand='lg' style={{ width: "100%" }}>
                        <Navbar.Brand href="#home" className="text-white" style={{ marginLeft: "70px" }}>Home</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className='mr-auto'>


                            </Nav>

                        </Navbar.Collapse>
                    </Navbar>
                </div>
            </>
        )
    }
}
export default HeaderComponent