import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import banana from '../../assets/images/bananaa.webp'
import cinnamonrolll from '../../assets/images/cinnamonroll.jpg'
import apple from '../../assets/images/apple.jpg'
import mincefMeat from '../../assets/images/mincedmeat.jpg'
import milk from '../../assets/images/milk.webp'
import organicMilk from '../../assets/images/organicmilk.webp'
import organicApple from '../../assets/images/greenapple.webp'
import groundBeef from '../../assets/images/groundbeef.jpg'
import steak from '../../assets/images/steak.jpg'


class ContentComponent extends Component {
    render() {
        return (
            <>
                <div className="container mt-3">
                    <h4>Product List</h4>
                    <p>Showing 1 - 9 of 24 products</p>
                    <div className="product-cards d-flex flex-wrap">
                        <Card className="mx-5 my-4" style={{ width: '18rem', boxShadow: " 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                            <Card.Header className="text-center text-primary" style={{ fontSize: "30px", fontWeight: "600" }}>Banana</Card.Header>
                            <Card.Body className="text-center" style={{ height: " 13rem" }}>
                                <Card.Img variant="top" src={banana} style={{ width: "12rem", margin: "0 auto" }} />
                            </Card.Body>

                            <Card.Body>

                                <Card.Text style={{ display: "inline-block", wordWrap: "break-word", overflow: "hidden", textOverflow: "ellipsis", maxHeight: "4.5em" }}>
                                    The banana is an edible fruit, botanically a berry, produced by several kinds of large herbaceous flowering plants in the genus Musa. In some countries, bananas used for cooking may be called plantains. The fruit is variable in size, color and firmness, but is usually elongated and curved, with soft flesh rich in starch covered with a rind which may be green, yellow, red, purple, or brown when ripe. The fruits grow in clusters hanging from the top of the plant. Almost all modern edible parthenocarpic (seedless) bananas come from two wild species - Musa acuminata and Musa balbisiana. The scientific names of most cultivated bananas are Musa acuminata, Musa balbisiana, and Musa x paradisiaca for the hybrid Musa acuminata x M. balbisiana, depending on their genomic constitution. The old scientific name Musa sapientum is no longer used. It is also yellow.
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Category: </a>Fruit
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Made by: </a>The banana company
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Organic: </a>Yes
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Price: </a>$7
                                </Card.Text>
                                <Card.Text className="text-center">
                                    <Button variant="primary">Add to card</Button>
                                </Card.Text>

                            </Card.Body>
                        </Card>

                        <Card className="mx-5 my-4" style={{ width: '18rem', boxShadow: " 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                            <Card.Header className="text-center text-primary" style={{ fontSize: "30px", fontWeight: "600" }}>Cinnamon roll</Card.Header>
                            <Card.Body className="text-center" style={{ height: " 13rem" }}>
                                <Card.Img variant="top" src={cinnamonrolll} style={{ width: "14rem", margin: "2 auto" }} />
                            </Card.Body>

                            <Card.Body>

                                <Card.Text style={{ display: "inline-block", wordWrap: "break-word", overflow: "hidden", textOverflow: "ellipsis", maxHeight: "4.5em" }}>
                                    Wheat flour,Cinnamon,Bunnies",
                                    "Description": "A cinnamon roll (also cinnamon bun, cinnamon swirl, cinnamon Danish and cinnamon snail) is a sweet roll served commonly in Northern Europe and North America. In North America its common use is for breakfast. Its main ingredients are flour, cinnamon, sugar, and butter, which provide a robust and sweet flavor. In some places it is eaten as a breakfast food and is often served with cream cheese or icing.
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Category: </a>Cookies and sweets
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Made by: </a>Awesome bakery
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Organic: </a>No
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Price: </a>$5
                                </Card.Text>
                                <Card.Text className="text-center">
                                    <Button variant="primary">Add to card</Button>
                                </Card.Text>

                            </Card.Body>
                        </Card>

                        <Card className="mx-5 my-4" style={{ width: '18rem', boxShadow: " 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                            <Card.Header className="text-center text-primary" style={{ fontSize: "30px", fontWeight: "600" }}>Apple</Card.Header>
                            <Card.Body className="text-center" style={{ height: " 13rem" }}>
                                <Card.Img variant="top" src={apple} style={{ width: "14rem", margin: "2 auto" }} />
                            </Card.Body>

                            <Card.Body>

                                <Card.Text style={{ display: "inline-block", wordWrap: "break-word", overflow: "hidden", textOverflow: "ellipsis", maxHeight: "4.5em" }}>
                                    The apple tree (Malus domestica) is a deciduous tree in the rose family best known for its sweet, pomaceous fruit, the apple. It is cultivated worldwide as a fruit tree, and is the most widely grown species in the genus Malus. The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today. Apples have been grown for thousands of years in Asia and Europe, and were brought to North America by European colonists. Apples have religious and mythological significance in many cultures, including Norse, Greek and European Christian traditions.
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Category: </a>Fruit
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Made by: </a>Fruits n Veggies
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Organic: </a>Yes
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Price: </a>$6
                                </Card.Text>
                                <Card.Text className="text-center">
                                    <Button variant="primary">Add to card</Button>
                                </Card.Text>

                            </Card.Body>
                        </Card>

                        <Card className="mx-5 my-4" style={{ width: '18rem', boxShadow: " 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                            <Card.Header className="text-center text-primary" style={{ fontSize: "30px", fontWeight: "600" }}>Minced meat</Card.Header>
                            <Card.Body className="text-center">
                                <Card.Img variant="top" src={mincefMeat} style={{ width: "14rem", margin: "2 auto" }} />
                            </Card.Body>

                            <Card.Body>

                                <Card.Text style={{ display: "inline-block", wordWrap: "break-word", overflow: "hidden", textOverflow: "ellipsis", maxHeight: "4.5em" }}>
                                    Ground beef, beef mince, minced meat, hamburger (in the United States) is a ground meat made of beef, finely chopped by a meat grinder. It is used in many recipes including hamburgers and cottage pie. In some parts of the world, a meat grinder is called a mincer.
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Category: </a>Meat
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Made by: </a>UltraBovine
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Organic: </a>No
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Price: </a>$6
                                </Card.Text>
                                <Card.Text className="text-center">
                                    <Button variant="primary">Add to card</Button>
                                </Card.Text>

                            </Card.Body>
                        </Card>

                        <Card className="mx-5 my-4" style={{ width: '18rem', boxShadow: " 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                            <Card.Header className="text-center text-primary" style={{ fontSize: "30px", fontWeight: "600" }}>Milk</Card.Header>
                            <Card.Body className="text-center">
                                <Card.Img variant="top" src={milk} style={{ width: "14rem", margin: "2 auto" }} />
                            </Card.Body>

                            <Card.Body>

                                <Card.Text style={{ display: "inline-block", wordWrap: "break-word", overflow: "hidden", textOverflow: "ellipsis", maxHeight: "4.5em" }}>
                                    Milk is a white liquid produced by the mammary glands of mammals. It is the primary source of nutrition for young mammals before they are able to digest other types of food. Early-lactation milk contains colostrum, which carries the mother's antibodies to its young and can reduce the risk of many diseases. Milk contains many other nutrients and the carbohydrate lactose.
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Category: </a>Dairy
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Made by: </a>Early
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Organic: </a>No
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Price: </a>$5
                                </Card.Text>
                                <Card.Text className="text-center">
                                    <Button variant="primary">Add to card</Button>
                                </Card.Text>

                            </Card.Body>
                        </Card>

                        <Card className="mx-5 my-4" style={{ width: '18rem', boxShadow: " 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                            <Card.Header className="text-center text-primary" style={{ fontSize: "30px", fontWeight: "600" }}>Organic Milk</Card.Header>
                            <Card.Body className="text-center">
                                <Card.Img variant="top" src={organicApple} style={{ width: "14rem", margin: "2 auto" }} />
                            </Card.Body>

                            <Card.Body>

                                <Card.Text style={{ display: "inline-block", wordWrap: "break-word", overflow: "hidden", textOverflow: "ellipsis", maxHeight: "4.5em" }}>
                                    The apple tree (Malus domestica) is a deciduous tree in the rose family best known for its sweet, pomaceous fruit, the apple. It is cultivated worldwide as a fruit tree, and is the most widely grown species in the genus Malus. The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today. Apples have been grown for thousands of years in Asia and Europe, and were brought to North America by European colonists. Apples have religious and mythological significance in many cultures, including Norse, Greek and European Christian traditions.
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Category: </a>Fruit
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Made by: </a>The banana company
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Organic: </a>Yes
                                </Card.Text>
                                <Card.Text><a style={{ fontSize: "18px", fontWeight: "700" }}>Price: </a>$7
                                </Card.Text>
                                <Card.Text className="text-center">
                                    <Button variant="primary">Add to card</Button>
                                </Card.Text>

                            </Card.Body>
                        </Card>

                        
                        


                    </div>

                </div>
            </>
        )
    }
}
export default ContentComponent