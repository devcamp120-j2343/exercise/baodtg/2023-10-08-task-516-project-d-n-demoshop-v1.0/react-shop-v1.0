import './App.css';
import ContentComponent from './components/contentComponents/content.component';
import FooterComponent from './components/footerComponents/footer.component';
import HeaderComponent from './components/headerComponents/header.component';

function App() {
  return (
    <div className="">
      <HeaderComponent />
      <ContentComponent />
      <FooterComponent />
    </div>
  );
}

export default App;
